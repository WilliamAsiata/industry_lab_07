package ictgradschool.industry.lab07.ex05;

/**
 * Created by wasi131 on 4/04/2017.
 */
public class IndexTooLowException extends Exception {
    public IndexTooLowException() {
    }

    public IndexTooLowException(String message) {
        super(message);
    }

    public IndexTooLowException(Throwable cause) {
        super(cause);
    }

    public IndexTooLowException(String message, Throwable cause) {
        super(message, cause);
    }
}