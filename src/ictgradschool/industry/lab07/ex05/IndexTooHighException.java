package ictgradschool.industry.lab07.ex05;

/**
 * Created by wasi131 on 4/04/2017.
 */
public class IndexTooHighException extends Exception {
    public IndexTooHighException() {
    }

    public IndexTooHighException(String message) {
        super(message);
    }

    public IndexTooHighException(Throwable cause) {
        super(cause);
    }

    public IndexTooHighException(String message, Throwable cause) {
        super(message, cause);
    }
}