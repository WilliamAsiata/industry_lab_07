package ictgradschool.industry.lab07.ex05;

/**
 * Created by wasi131 on 4/04/2017.
 */
public class InvalidIndexException extends Exception {
    public InvalidIndexException() { }

    public InvalidIndexException(String message) { super(message); }

    public InvalidIndexException(Throwable cause) { super(cause); }

    public InvalidIndexException(String message, Throwable cause) { super(message, cause); }
}
