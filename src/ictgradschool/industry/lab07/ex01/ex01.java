package ictgradschool.industry.lab07.ex01;

/**
 * Created by wasi131 on 3/04/2017.
 */
public class ex01 {

    public static void main(String[] args) {

        ex01 m = new ex01();
        m.start();
    }

    public void start() {
//        tryCatch06();
//        throwsClause09();
        throwsClause10();
    }

    private void tryCatch01() {
        int result = 0;
        int[] nums = null; /*new int[3];*/
        try {
            result = nums.length;
            System.out.println("See you");
        } catch (ArithmeticException e) {
            System.out.println("Problem");
            result = -1;
        }
        System.out.println("Result: " + result);
    }

    private void tryCatch06() {
        try {
            try06(0, "");
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println("B Error");
        }
    }

    private void try06(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            num = 200 / num;
        } catch (NullPointerException e) {
            System.out.println("E Error");
        }
        System.out.println("F");
    }

    private void throwsClause09() {
        try {
            throws09(null);
            System.out.println("A");
        } catch (NullPointerException e) {
            System.out.println(e);
        }
        System.out.println("B");
    }

    private void throws09(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Null String");
        }
        System.out.println("C");
    }

    private void throwsClause10() {
        try {
            throws10(null);
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println(e);
        } finally {
            System.out.println("B");
        }
        System.out.println("C");
    }

    private void throws10(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Bad String");
        }
        System.out.println("D");
    }







}
