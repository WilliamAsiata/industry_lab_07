package ictgradschool.industry.lab07.ex06;

/**
 * Created by wasi131 on 4/04/2017.
 */
public class ExceedMaxStringLengthException extends Exception {
    public ExceedMaxStringLengthException() {
    }

    public ExceedMaxStringLengthException(String message) {
        super(message);
    }

    public ExceedMaxStringLengthException(Throwable cause) {
        super(cause);
    }

    public ExceedMaxStringLengthException(String message, Throwable cause) {
        super(message, cause);
    }
}