package ictgradschool.industry.lab07.ex06;

/**
 * Created by wasi131 on 4/04/2017.
 */
public class InvalidWordException extends Exception {
    public InvalidWordException() {
    }

    public InvalidWordException(String message) {
        super(message);
    }

    public InvalidWordException(Throwable cause) {
        super(cause);
    }

    public InvalidWordException(String message, Throwable cause) {
        super(message, cause);
    }
}