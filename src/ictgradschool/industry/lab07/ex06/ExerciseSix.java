package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;

/**
 * Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        // Write the codes :)

        boolean worked = false;
        String initials;
        while (!worked){
            try {
                String word = getStringFromUser();
                initials = getInitials(word);
                worked = true;
                System.out.print("You entered:" + initials);
            } catch (InvalidWordException | ExceedMaxStringLengthException e){
                System.out.println(e);
            } catch (StringIndexOutOfBoundsException e){
                System.out.println( e + "\nNo string entered!");
//                e.printStackTrace();
            }
        }
    }

    private String getInitials(String word) throws InvalidWordException {

        String initials = "";

        String[] array = word.split(" ");
        for (String s : array) {
            char c = s.charAt(0);
            if (!Character.isLetter(c)) {
                throw new InvalidWordException("Strings entered do not begin with a letter!");
            }
            initials += " " + c;
        }
        return initials;

//        char initChar = word.charAt(0);
//
//        if (!Character.isLetter(initChar)){
//            throw new InvalidWordException("Strings entered do not begin with a letter!");
//        }
//
//        String initial = " " + initChar;
//
//        int nextIndex;
//        int len = word.length();
//
//        if (word.contains(" ")){
//            nextIndex = word.indexOf(" ");
//            while (Character.isSpaceChar(word.charAt(nextIndex))){
//                if (nextIndex++ == len - 1){
//                    return initial;
//                }
//            }
//        } else {
//            return initial;
//        }
//
//        return initial + getInitials(word.substring(nextIndex, len));
    }

    private String getStringFromUser() throws ExceedMaxStringLengthException {

        System.out.print("Enter a string of at most 100 characters: ");

        String word = Keyboard.readInput();

        if (100 < word.length()){
            throw new ExceedMaxStringLengthException("Input has too many characters!");
        }

        return word;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }


}
