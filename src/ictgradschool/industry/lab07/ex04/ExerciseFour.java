package ictgradschool.industry.lab07.ex04;

import ictgradschool.Keyboard;

public class ExerciseFour
{
    /**
     * Main program entry point. Do not edit, except for uncommenting the marked lines when required.
     */
	public static void main(String[] args)
	{
		ExerciseFour program = new ExerciseFour();
		
		// Exercise four, Question 1
		program.divideNumbers();

        // Exercise four, Question 2
        // You may uncomment this line to help test your solution to question two.
		 program.question2();

        // Exercise four, Question 3
        // You may uncomment this line to help test your solution to question three.
		 program.question3();
	}

	/**
	 * The following tries to divide using two user input numbers, but is prone to error.
     *
     * Add some error handling to the code as follows:
     * 1) If the user enters 0 for the second number, "Divide by 0!" should be printed instead of crashing the program.
     * 2) If the user enters numbers which aren't integers, "Input error!" should be printed instead of crashing the program.
     * Both these exceptional cases should be handled in the same try-catch block.
	 */
	public void divideNumbers() {

		try {
			System.out.print("Enter the first number: ");
			String str1 = Keyboard.readInput();
			int num1 = Integer.parseInt(str1);
			System.out.print("Enter the second number: ");
			String str2 = Keyboard.readInput();
			int num2 = Integer.parseInt(str2);

			// Output the result
			System.out.println(num1 + " / " + num2 + " = " + (num1 / num2));
		} catch (NumberFormatException e){
			System.out.println("Input error!");
		} catch (ArithmeticException e){
			System.out.println("Division by 0!");
		}
	}

	public void question2() {
		//Write some Java code which throws a StringIndexOutOfBoundsException
		String b = "qbb b rgnb qr n";
		char c = b.charAt(100);

//		throw new StringIndexOutOfBoundsException("");

	}
	
	public void question3() {
		//Write some Java code which throws a ArrayIndexOutOfBoundsException
		int[] a = new int[10];
		a[1] = a[10];

//		throw new ArrayIndexOutOfBoundsException("");
	}
}

